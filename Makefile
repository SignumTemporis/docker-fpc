MAINTAINER := Signum Temporis <7721809-SignumTemporis@users.noreply.gitlab.com>
PROJECT_URL := https://gitlab.com/SignumTemporis/docker-fpc
TITLE := Free Pascal Compiler (FPC)

include settings.sh

BATS := bats
.DEFAULT_GOAL := builds


LABELS := \
	--label "org.opencontainers.image.authors=$(MAINTAINER)" \
	--label "org.opencontainers.image.url=$(PROJECT_URL)" \
	--label "org.opencontainers.image.source=$(PROJECT_URL).git" \
	--label "org.opencontainers.image.title=$(TITLE)" \
	--label "org.opencontainers.image.version=$(FPC_VERSION)"

PUBLIC_TAGS_fpc-main-full := \
	latest $(FPC_VERSION) \
	full $(FPC_VERSION)-full
PUBLIC_TAGS_fpc-main-slim := \
	slim $(FPC_VERSION)-slim
PUBLIC_TAGS_fpc-cross-win64-full := \
	cross.x86_64-win64 $(FPC_VERSION)-cross.x86_64-win64 \
	cross.x86_64-win64.full $(FPC_VERSION)-cross.x86_64-win64.full
PUBLIC_TAGS_fpc-cross-win64-slim := \
	cross.x86_64-win64.slim $(FPC_VERSION)-cross.x86_64-win64.slim

intermediates := fpc-base fpc-main-install fpc-cross-win64-install
targets := fpc-main-full fpc-main-slim fpc-cross-win64-full fpc-cross-win64-slim
cleanTargets := $(addsuffix .clean,$(targets) $(intermediates))
tagTargets := $(addsuffix .tag,$(targets))
testTargets := $(addsuffix .test,$(targets))
pushTargets := $(addsuffix .push,$(targets))
cleantagTargets := $(addsuffix .cleantag,$(targets))


###############################################################################
# Composite goals
###############################################################################

# Build all target images
.PHONY: builds
builds: $(targets)

# Remove all local tags of images (including intermediates)
.PHONY: clean
clean: $(cleanTargets)

# Assigne all public tags to target images
.PHONY: tags
tags: $(tagTargets)

# Test all target images
.PHONY: tests
tests: $(testTargets)

# Push all public tags of target images
.PHONY: push
push: $(pushTargets)

# Remove all public tags of target images
.PHONY: cleantags
cleantags: $(cleantagTargets)

# Remove all local and public tags
.PHONY: cleanall
cleanall: clean cleantags


###############################################################################
# Sub-goals to build intermediate images
###############################################################################

.PHONY: $(intermediates)

fpc-base:
	docker build \
		--build-arg ALPINE_TAG=$(ALPINE_TAG) \
		-t $@:latest src/base

fpc-main-install: fpc-base
fpc-cross-win64-install: fpc-main-install
$(filter fpc-%-install,$(intermediates)): fpc-%-install:
	docker build \
		--build-arg ALPINE_TAG=$(ALPINE_TAG) \
		--build-arg FPC_VERSION=$(FPC_VERSION) \
		-t $@:latest src/$*


###############################################################################
# Sub-goals to build target images
###############################################################################

define targetBuildTemplate =
.PHONY: fpc-$(1)-$(2)
fpc-$(1)-$(2): fpc-$(1)-install fpc-base
	docker build \
		--build-arg IMG_INSTALL=$$<:latest \
		$(LABELS) \
		-t $$@:latest src/$(2)
endef

$(eval $(call targetBuildTemplate,main,full))
$(eval $(call targetBuildTemplate,main,slim))
$(eval $(call targetBuildTemplate,cross-win64,full))
$(eval $(call targetBuildTemplate,cross-win64,slim))


###############################################################################
# Other sub-goals
###############################################################################

# Remove local tag of an image (including intermediates)
.PHONY: $(cleanTargets)
$(cleanTargets): %.clean:
	docker rmi -f $*:latest

# Assigne public tags to a target image
.PHONY: $(tagTargets)
$(tagTargets): %.tag:
	for t in $(PUBLIC_TAGS_$*); do \
		docker tag $*:latest $(PUBLIC_NAME):$$t; \
	done

# Test a target image
.PHONY: $(testTargets)
$(testTargets): %.test:
	$(BATS) test/$*.bats

# Push public tags of a target image
.PHONY: $(pushTargets)
$(pushTargets): %.push:
	for t in $(PUBLIC_TAGS_$*); do \
		docker push $(PUBLIC_NAME):$$t; \
	done

# Remove public tags of a target image
.PHONY: $(cleantagTargets)
$(cleantagTargets): %.cleantag:
	for t in $(PUBLIC_TAGS_$*); do \
		docker rmi -f $(PUBLIC_NAME):$$t; \
	done
